package com.bans.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.bans.commands.BanCommand;
import com.bans.commands.CheckBanCommand;
import com.bans.commands.IPBanCommand;
import com.bans.commands.KickAllCommand;
import com.bans.commands.KickCommand;
import com.bans.commands.ReloadBansCommand;
import com.bans.commands.TempBanCommand;
import com.bans.commands.UnBanIPCommand;
import com.bans.commands.UnbanCommand;

public class BungeeSuiteBans extends JavaPlugin {

    public static String OUTGOING_PLUGIN_CHANNEL = "BSBans";
    public static String INCOMING_PLUGIN_CHANNEL = "BungeeSuiteBans";
    public static BungeeSuiteBans instance;

    @Override
    public void onEnable() {
        instance = this;
        registerChannels();
        registerCommands();
    }

    private void registerCommands() {
        getCommand( "ban" ).setExecutor( new BanCommand() );
        getCommand( "checkban" ).setExecutor( new CheckBanCommand() );
        getCommand( "ipban" ).setExecutor( new IPBanCommand() );
        getCommand( "kick" ).setExecutor( new KickCommand() );
        getCommand( "kickall" ).setExecutor( new KickAllCommand() );
        getCommand( "reloadbans" ).setExecutor( new ReloadBansCommand() );
        getCommand( "tempban" ).setExecutor( new TempBanCommand() );
        getCommand( "unban" ).setExecutor( new UnbanCommand() );
        getCommand( "unipban" ).setExecutor( new UnBanIPCommand() );
    }

    private void registerChannels() {
        this.getServer().getMessenger().registerOutgoingPluginChannel( this, OUTGOING_PLUGIN_CHANNEL );
        Bukkit.getMessenger().registerIncomingPluginChannel( this, INCOMING_PLUGIN_CHANNEL, new BansMessageListener() );
    }


}
